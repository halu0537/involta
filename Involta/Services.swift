//
//  Services.swift
//  Involta
//
//  Created by Радим Гасанов on 14.03.2021.
//

import UIKit

class ServiceManager {
    class func jsonGet(
        urlString: String,
        complection: @escaping (
            _ dict: [String: Any]?,
            _ data: Data?,
            _ errors: String?
        ) -> ()
    ) {
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(
            "application/json",
            forHTTPHeaderField: "Content-Type"
        )
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
        guard let data = data else { return }
            
        do {
            let jsonDict = try? JSONSerialization.jsonObject(with: data,
                                                            options: []) as? [String: Any]

            complection(jsonDict,
                        data,
                        nil)
            } catch {
                complection(nil,
                            data,
                            error.localizedDescription)
            }
        }.resume()
    }
    
    class func jsonArrayGet(
        urlString: String,
        complection: @escaping (
            _ dictArray: [[String: Any]]?,
            _ data: Data?,
            _ errors: String?
        ) -> ()
    ) {
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(
            "application/json",
            forHTTPHeaderField: "Content-Type"
        )
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
        guard let data = data else { return }
            
        do {
            let jsonDict = try? JSONSerialization.jsonObject(with: data,
                                                            options: []) as? [[String: Any]]

            complection(jsonDict,
                        data,
                        nil)
            } catch {
                complection(nil,
                            data,
                            error.localizedDescription)
            }
        }.resume()
    }
}


