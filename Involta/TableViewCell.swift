//
//  TableViewCell.swift
//  Involta
//
//  Created by Радим Гасанов on 14.03.2021.
//

import UIKit

class TableViewCell: UITableViewCell {

    static let reuseIdentifier = "TableViewCellReuseIdentifier"
    
    private let idLabel: UILabel = {
        let item = UILabel()
        item.translatesAutoresizingMaskIntoConstraints = false
        return item
    }()

    private let setupLabel: UILabel = {
        let item = UILabel()
        item.numberOfLines = 0
        item.translatesAutoresizingMaskIntoConstraints = false
        return item
    }()
    
    private let punchlineLabel: UILabel = {
        let item = UILabel()
        item.numberOfLines = 0
        item.translatesAutoresizingMaskIntoConstraints = false
        return item
    }()
    
    private let typeLabel: UILabel = {
        let item = UILabel()
        item.translatesAutoresizingMaskIntoConstraints = false
        return item
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        selectionStyle = .none
        addSubview(idLabel)
        idLabel.topAnchor ~= self.topAnchor + 5
        idLabel.leadingAnchor ~= self.leadingAnchor + 5
        
        addSubview(typeLabel)
        typeLabel.topAnchor ~= self.topAnchor + 5
        typeLabel.trailingAnchor ~= self.trailingAnchor - 5
        
        addSubview(setupLabel)
        setupLabel.topAnchor ~= typeLabel.bottomAnchor + 5
        setupLabel.leadingAnchor ~= self.leadingAnchor + 5
        setupLabel.trailingAnchor ~= self.trailingAnchor - 5
        setupLabel.bottomAnchor ~= self.centerYAnchor - 5

        addSubview(punchlineLabel)
        punchlineLabel.topAnchor ~= self.centerYAnchor + 5
        punchlineLabel.leadingAnchor ~= self.leadingAnchor + 5
        punchlineLabel.trailingAnchor ~= self.trailingAnchor - 5
        punchlineLabel.bottomAnchor ~= self.bottomAnchor - 5
    }

    public func setModel(_ model: Joke) {
        idLabel.text = "id: \(model.id)"
        setupLabel.text = model.setup
        punchlineLabel.text = model.punchline
        typeLabel.text = "type: \(model.type.rawValue)"
    }
}
