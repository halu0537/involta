//
//  TableViewController.swift
//  Involta
//
//  Created by Радим Гасанов on 14.03.2021.
//

import UIKit

class TableViewController: UITableViewController {
    
    private static let reuseIdentifier = "reuseIdentifier"
    
    private var models: [Joke] = []
    
//    private var models = [ImageModel]()
    private var recordsArray = [Int]()
    private var page = 1
    private var limit = 10
    let totalEnteries = 500
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseIdentifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: TableViewController.reuseIdentifier)
        
        downloadJokes(page)
        var index = 0
        while index < limit {
            recordsArray.append(index)
            index += 1
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }
    
    @objc private func loadCollection() {
        tableView.reloadData()
        guard recordsArray.count - models.count < 20 else { return }
        page += 1
        downloadJokes(page)
    }
    
    private func downloadJokes(_ page: Int) {
        ServiceManager.jsonArrayGet(urlString: "https://official-joke-api.appspot.com/jokes/ten") { (dictArray, data, error) in
            var jokes: [Joke] = []
            guard let dictArray = dictArray else { return }
            dictArray.forEach { (dict) in
                if
                    let id = dict["id"] as? Int,
                    let punchline = dict["punchline"] as? String,
                    let setup = dict["setup"] as? String,
                    let typeValue = dict["type"] as? String,
                    let type = Type.init(rawValue: typeValue)
                {
                    
                    jokes.append(.init(
                                    id: id,
                                    punchline: punchline,
                                    setup: setup,
                                    type: type
                                )
                    )
                }
                self.models += jokes
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return min(recordsArray.count, models.count)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: TableViewCell.reuseIdentifier,
                for: indexPath
            ) as? TableViewCell else {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: TableViewController.reuseIdentifier,
                for: indexPath
            )
            cell.backgroundColor = .red
            cell.textLabel?.text = "cant cust \(indexPath)"
            return cell
        }
        cell.setModel(models[indexPath.row])
        return cell
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard
            indexPath.row == recordsArray.count - 1,
            recordsArray.count < totalEnteries
        else {
            return
        }
        var index = recordsArray.count
        limit = index + 10
        while index < limit {
            recordsArray.append(index)
            index += 1
        }
        self.perform(#selector(loadCollection), with: nil, afterDelay: 2.0)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
