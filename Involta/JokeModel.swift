//
//  JokeModel.swift
//  Involta
//
//  Created by Радим Гасанов on 15.03.2021.
//

import Foundation

struct Joke: Codable {
    var id: Int
    var punchline: String
    var setup: String
    
    var type: Type
}

enum Type: String, Codable {
    case general
    case programming
}
