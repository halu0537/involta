//
//  ViewController.swift
//  Involta
//
//  Created by Радим Гасанов on 13.03.2021.
//

import UIKit

class ViewController: UIViewController {
    
    private let UrlStrings = [
        "https://img3.goodfon.ru/original/320x240/2/c1/kot-koshka-mordochka-glazischa.jpg",
        "https://crosti.ru/patterns/00/09/e1/e32d5d3650/preview.jpg",
        "https://img1.goodfon.ru/original/320x240/c/80/kot-seryy-morda-vzglyad-priroda.jpg",
        "https://wallbox.ru/resize/800x480/wallpapers/main/201601/a722441072774f0.jpg",
        "https://img3.goodfon.ru/original/320x240/6/a2/kot-polosatyy-koshka-lezhit.jpg",
        "https://f.vividscreen.info/soft/1055fa7566402161892d5022631a5d34/Lovely-Kitten-320x240.jpg",
        "https://dl.backbook.me/full/105db0cd08.jpg",
        "https://animals.kharkov.ua/files/animals/images%20%283%29.jpg",
        "https://sun9-43.userapi.com/impf/c855620/v855620609/23112e/F_DTAX07QN8.jpg?size=512x320&quality=96&proxy=1&sign=6598968b1db7170484407e3ad18a6245&type=album",
        "https://i.pinimg.com/736x/74/55/5a/74555a885413dd930b21ab9beb86c4d2--domestic-cat-africans.jpg",
        "https://i.ytimg.com/vi/nXiAosV4zpU/sddefault.jpg"
    ]
    
    private let imageView: UIImageView = {
        let item = UIImageView()
        item.backgroundColor = .lightGray
        return item
    }()
    
    private let leftButton: UIButton = {
        let item = UIButton()
        item.setTitle("Random Joke", for: .normal)
        item.setTitleColor(.black, for: .normal)
        item.setTitleColor(.lightGray, for: .highlighted)
        item.layer.borderWidth = 2
        item.layer.cornerRadius = 10
        item.layer.borderColor = UIColor.lightGray.cgColor
        item.addTarget(self, action: #selector(randomJoke), for: .touchUpInside)
        item.alpha = 0
        return item
    }()
    
    private let rightButton: UIButton = {
        let item = UIButton()
        item.setTitle("Jokes", for: .normal)
        item.setTitleColor(.black, for: .normal)
        item.setTitleColor(.lightGray, for: .highlighted)
        item.layer.borderWidth = 2
        item.layer.cornerRadius = 10
        item.layer.borderColor = UIColor.lightGray.cgColor
        item.addTarget(self, action: #selector(Jokes), for: .touchUpInside)
        item.alpha = 0
        return item
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupImageView()
        setupButtons()
    }
    
    private func setupImageView() {
        
        imageView.frame = CGRect(
            x: 0,
            y: 100,
            width:  view.bounds.width - 100,
            height: view.bounds.width - 100
        )
        imageView.center.x = view.center.x
        view.addSubview(imageView)
        
        setImage()
    }
    
    private func setImage() {
        guard let urlString = UrlStrings.randomElement() else { return }
        imageView.loadImageUsingCacheWithUrlStringEXT(urlString: urlString)
    }
    
    private func setupButtons() {
        let offset = view.bounds.width / 20
        let width = view.bounds.width / 2 - 2 * offset
        
        leftButton.frame = CGRect(
            x: offset,
            y: view.bounds.height,
            width: width,
            height: offset
        )
        view.addSubview(leftButton)
        
        rightButton.frame = CGRect(
            x: view.center.x + offset,
            y: view.bounds.height,
            width: width,
            height: offset
        )
        view.addSubview(rightButton)
        
        UIView.animate(withDuration: 2) {
            self.leftButton.alpha = 1
            self.rightButton.alpha = 1
            
            self.leftButton.center.y = self.view.center.y
            self.rightButton.center.y = self.view.center.y
        }
    }
    
    @objc private func randomJoke() {
        ServiceManager.jsonGet(urlString: "https://official-joke-api.appspot.com/random_joke") { (dict, data, error) in
            var title: String
            var message: String
            if let data = data {
                let joke: Joke = try! JSONDecoder().decode(Joke.self, from: data)
                title = joke.setup
                message = joke.punchline
            } else {
                title = "Watning"
                message = "Can`t download joke!"
            }
            DispatchQueue.main.async {
                let alertController = UIAlertController(
                    title: title,
                    message: message,
                    preferredStyle: .alert
                )
                let action = UIAlertAction(title: "Ok", style: .default)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc private func Jokes() {
        let vc = TableViewController()
        present(vc, animated: true, completion: nil)
    }
    
}
